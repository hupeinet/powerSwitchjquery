# powerSwitchjquery

#### 项目介绍
jquery选项卡切换与跑马灯图片滚动组合功能效果
<script type="text/javascript" src="js/jquery.pack.js"></script>
<script type="text/javascript" src="js/jquery.SuperSlide.js"></script>
<script type="text/javascript">
    /*
    SuperSlide组合注意：
    1、内外层mainCell、targetCell、prevCell、nextCell等对象不能相同，除非特殊应用；
    2、注意书写顺序，通常先写内层js调用，再写外层js调用
    */
    /* 内层图片滚动切换 */
    jQuery(".slideGroup .slideBox").slide({
        mainCell:"ul",
        vis:3,
        prevCell:".sPrev",
        nextCell:".sNext",
        effect:"leftLo

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)